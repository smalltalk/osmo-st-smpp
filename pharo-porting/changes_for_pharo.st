
SMPPConnection extend [
    createConnection: aHostname port: aPort [
        <category: 'socket'>
        ^(SocketStream openConnectionToHostNamed: aHostname port: aPort)
            binary;
            noTimeout;
            yourself
    ]
]

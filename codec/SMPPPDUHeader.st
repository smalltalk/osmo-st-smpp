"
 (C) 2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: SMPPPDUHeader [
    | commandId commandStatus sequenceNumber |
    <category: 'SMPP-Codec'>
    <comment: 'I represent a SMPPv3.4 SMPP PDU Header. E.g.
    3.2 of the SMPPv3.4 specification. The four byte lengths
    need to be written/read by someone else.'>

    SMPPPDUHeader class >> readFrom: aStream [
        ^self new
            commandId: ((aStream next: 4) uintAt: 1) swap32;
            commandStatus: ((aStream next: 4) uintAt: 1) swap32;
            sequenceNumber: ((aStream next: 4) uintAt: 1) swap32;
            yourself
    ]

    commandId [
        <category: 'accessing'>
        ^commandId
    ]

    commandStatus [
        <category: 'accessing'>
        ^commandStatus
    ]

    sequenceNumber [
        <category: 'accessing'>
        ^sequenceNumber
    ]

    commandId: anId [
        commandId := anId
    ]

    commandStatus: aStatus [
        commandStatus := aStatus
    ]

    sequenceNumber: aNumber [
        sequenceNumber := aNumber
    ]

    writeOn: aMsg [
        aMsg
            putLen32: commandId;
            putLen32: commandStatus;
            putLen32: sequenceNumber
    ]
]

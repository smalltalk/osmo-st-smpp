"
 (C) 2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

SMPPBodyBase subclass: SMPPBindTransmitterBody [
    | system_id password system_type version addr_ton addr_npi addr_range |
    <category: 'SMPP-Codec'>

    SMPPBindTransmitterBody class >> messageType [
        ^self bindTransmitter
    ]

    SMPPBindTransmitterBody class >> tlvDescription [
        ^OrderedCollection new
            add: SMPPSystemId tlvDescription;
            add: SMPPPassword tlvDescription;
            add: SMPPSystemType tlvDescription;
            add: SMPPInterfaceVersion tlvDescription;
            add: SMPPAddressTypeOfNumber tlvDescription;
            add: SMPPAddressNumberingPlanIndicator tlvDescription;
            add: SMPPAddressRange tlvDescription;
            yourself
    ]

    systemId [
        ^system_id
    ]

    password [
        ^password
    ]

    systemType [
        ^system_type
    ]

    version [
        ^0
    ]

    typeOfNumber [
        ^addr_ton
    ]

    numberingPlanIndicator [
        ^addr_npi
    ]

    addressRange [
        ^addr_range
    ]

    systemId: anId [
        system_id := anId
    ]

    password: aPassword [
        password := aPassword
    ]

    systemType: aType [
        system_type := aType
    ]

    version: aVersion [
        version := aVersion
    ]

    typeOfNumber: aNumber [
        addr_ton := aNumber
    ]

    numberingPlanIndicator: anIndicator [
        addr_npi := anIndicator
    ]

    addressRange: aRange [
        addr_range := aRange
    ]
]

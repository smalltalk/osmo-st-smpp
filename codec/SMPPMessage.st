"
 (C) 2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: SMPPMessage [
    | header body |

    <category: 'SMPP-Codec'>

    SMPPMessage class >> readFrom: aStream [
        | len data stream header body |
        len := ((aStream next: 4) asByteArray uintAt: 1) swap32.
        data := aStream next: len - 4.
        stream := data asByteArray readStream.

        header := SMPPPDUHeader readFrom: stream.
        body := SMPPBodyBase readFrom: stream for: header.
        ^SMPPMessage new
            header: header;
            body: body;
            yourself
    ]

    header: aHeader [
        header := aHeader
    ]

    header [
        ^header
    ]

    body: aBody [
        body := aBody
    ]

    body [
        ^body
    ]

    writeOn: aMsg [
        | hdrData bodyData  |
        hdrData := header toMessageOrByteArray.
        bodyData := body toMessageOrByteArray.

        aMsg
            putLen32: (hdrData size + bodyData size + 4);
            putByteArray: hdrData;
            putByteArray: bodyData.
    ]
]

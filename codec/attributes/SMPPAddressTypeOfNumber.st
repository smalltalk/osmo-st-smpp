"
 (C) 2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

SMPPInteger subclass: SMPPAddressTypeOfNumber [

    <category: 'SMPP-Codec'>
    <comment: 'I re-present 5.2.5 of SMPPv3.4'>

    SMPPAddressTypeOfNumber class [

        tonUnknown [
            <category: 'attribute'>
            ^2r000
        ]

        tonInternational [
            <category: 'attribute'>
            ^2r001
        ]

        tonNational [
            <category: 'attribute'>
            ^2r010
        ]

        tonNetworkSpecific [
            <category: 'attribute'>
            ^2r011
        ]

        tonSubscriberNumber [
            <category: 'attribute'>
            ^2r100
        ]

        tonAlphanumeric [
            <category: 'attribute'>
            ^2r101
        ]

        tonAbbreviated [
            <category: 'attribute'>
            ^2r110
        ]
    ]

    SMPPAddressTypeOfNumber class >> tlvDescription [
        ^super tlvDescription
            instVarName: #addr_ton;
            valueSize: 1;
            yourself
    ]
]

"
 (C) 2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

SMPPInteger subclass: SMPPAddressNumberingPlanIndicator [

    <category: 'SMPP-Codec'>
    <comment: 'I re-present 5.2.6 of SMPPv3.4'>

    SMPPAddressNumberingPlanIndicator class [
        npiUnknown [
            <category: 'attribute'>
            ^2r000
        ]

        npiISDN [
            <category: 'attribute'>
            ^2r001
        ]

        npiData [
            <category: 'attribute'>
            ^2r011
        ]

        npiTelex [
            <category: 'attribute'>
            ^2r100
        ]

        npiLandMobile [
            <category: 'attribute'>
            ^2r110
        ]

        npiNational [
            <category: 'attribute'>
            ^2r1000
        ]

        npiPrivate [
            <category: 'attribute'>
            ^2r1001
        ]

        npiERMES [
            <category: 'attribute'>
            ^2r1010
        ]

        npiInternet [
            <category: 'attribute'>
            ^2r1110
        ]

        npiWap [
            <category: 'attribute'>
            ^2r10010
        ]
    ]

    SMPPAddressNumberingPlanIndicator class >> tlvDescription [
        ^super tlvDescription
            instVarName: #addr_npi;
            valueSize: 1;
            yourself
    ]
]

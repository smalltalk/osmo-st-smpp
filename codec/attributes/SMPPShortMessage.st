"
 (C) 2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: SMPPShortMessage [

    <category: 'SMPP-Codec'>
    <comment: 'I represent the sm_length and short_message'>

    SMPPShortMessage class >> tlvDescription [
        ^Osmo.TLVDescription new
            beLV;
            instVarName: #short_message;
            minSize: 0 maxSize: 254;
            parseClass: self;
            yourself
    ]

    SMPPShortMessage class >> readFrom: aStream with: anAttribute [
        | len |
        len := aStream next.
        ^(aStream next: len) asString
    ]

    SMPPShortMessage class >> write: aValue on: aMsg with: anAttribute [
        aMsg
            putByte: aValue size;
            putByteArray: aValue asByteArray.
    ]
]

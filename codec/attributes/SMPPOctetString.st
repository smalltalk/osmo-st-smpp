"
 (C) 2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: SMPPOctetString [

    <category: 'SMPP-Codec'>
    <comment: 'I represent a variable string as used in SMPP'>

    SMPPOctetString class >> tlvDescription [
        ^Osmo.TLVDescription new
            instVarName: #string; parseClass: self;
            typeKind: Osmo.TLVDescription valueOnly;
            yourself
    ]

    SMPPOctetString class >> readFrom: aStream with: anAttribute [
        | str |
        str := WriteStream on: String new.
        [aStream peek = 0] whileFalse: [
            str nextPut: aStream next asCharacter].

        "Skip the $0 now"
        aStream next.

        "anAttribute... verify the max size"
        ^str contents
    ]

    SMPPOctetString class >> write: aValue on: aMsg with: anAttr [
        "Todo.. verify the size constraints..."
        aMsg
            putByteArray: aValue asByteArray;
            putByte: 0
    ]
]

"
 (C) 2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

SMPPInteger subclass: SMPPESMClass [

    <category: 'SMPP-Codec'>
    <comment: 'I re-present a 5.2.12'>

    SMPPESMClass class [
        modeBitMask [
            <category: 'ESME->SMSC'>
            ^2r11
        ]

        modeDefault [
            <category: 'ESME->SMSC'>
            ^2r00
        ]

        modeDatagram [
            <category: 'ESME->SMSC'>
            ^2r01
        ]

        modeForward [
            <category: 'ESME->SMSC'>
            ^2r10
        ]

        typeBitMask [
            <category: 'ESME->SMSC'>
            ^2r00111100
        ]

        typeDefault [
            <category: 'ESME or SMSC'>
            ^2r0 bitShift: 2
        ]

        typeSMSCDeliveryReceipt [
            <category: 'SMSC->ESME'>
            ^2r1 bitShift: 2
        ]

        typeESMEDeliveryAck [
            <category: 'ESME->SMSC'>
            ^2r10 bitShift: 2
        ]

        typeESMEUserAck [
            <category: 'ESME->SMSC'>
            ^2r100 bitShift: 2
        ]

        typeConversationAbort [
            <category: 'SMSC->ESME'>
            ^2r110 bitShift: 2
        ]

        typeIntermediateDeliveryNotification [
            <category: 'SMSC->ESME'>
            ^2r1000 bitShift: 2
        ]

        gsmBitMask [
            <category: 'ESME or SMSC'>
            ^2r11000000
        ]

        gsmNoSpecific [
            <category: 'ESME or SMSC'>
            ^2r00000000
        ]

        gsmUDHIIndicator [
            <category: 'ESME or SMSC'>
            ^2r01000000
        ]

        gsmReplyPath [
            <category: 'ESME or SMSC'>
            ^2r10000000
        ]

        gsmUHDIAndReplyPath [
            <category: 'ESME/SMSC'>
            ^self gsmUDHIIndicator bitOr: self gsmReplyPath
        ]
    ]

    SMPPESMClass class >> tlvDescription [
        ^super tlvDescription
            valueSize: 1;
            instVarName: #esm_class;
            yourself
    ]
]

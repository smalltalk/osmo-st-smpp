"
 (C) 2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

SMPPOctetString subclass: SMPPSystemType [

    <category: 'SMPP-Codec'>
    <comment: 'I re-present 5.2.3 of SMPPv3.4'>

    SMPPSystemType class >> tlvDescription [
        ^super tlvDescription
            instVarName: #system_type;
            minSize: 0 maxSize: 13;
            yourself
    ]
]

"
 (C) 2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: SMPPInteger [

    <category: 'SMPP-Codec'>
    <comment: 'I help with the various kind of numbers'>

    SMPPInteger class >> tlvDescription [
        ^Osmo.TLVDescription new
            parseClass: self;
            typeKind: Osmo.TLVDescription valueOnly;
            yourself
    ]

    SMPPInteger class >> readFrom: aStream with: anAttribute [
        anAttribute valueSize = 1
            ifTrue: [^aStream next].

        "This is not implemented yet"
        ^self error: 'The base class does not support other value sizes'.
    ]

    SMPPInteger class >> write: aValue on: aMsg with: anAttribute [
        anAttribute valueSize = 1
            ifTrue: [^aMsg putByte: aValue].
        ^self error: 'This value size is not supported yet.'
    ]
]

Object subclass: SMPPCommand [
    | body onTimeout onResult onError timeoutTimer connection |
    
    <category: 'SMPP-Connection'>
    <comment: ''>

    SMPPCommand class >> initWith: aBody [
        ^self new
            body: aBody;
            yourself
    ]

    messageType [
        ^body class messageType
    ]

    body [
        ^body
    ]

    body: aBody [
        <category: 'creation'>
        body := aBody
    ]

    onResult: aBlock [
        <category: 'creation'>
        onResult := aBlock
    ]

    onError: aBlock [
        <category: 'creation'>
        onError := aBlock
    ]

    onTimeout: aBlock [
        <category: 'creation'>
        onTimeout := aBlock
    ]

    scheduledOn: aConnection [
        connection := aConnection.
        timeoutTimer := Osmo.TimerScheduler instance
                            scheduleInSeconds: 10 block: [self timeout].
    ]

    timeout [
        <category: 'result dispatch'>
        onTimeout ifNotNil: [:block | block value].
    ]

    error: anError [
        <category: 'result dispatch'>
        timeoutTimer cancel.
        onError ifNotNil: [:block | block value: anError].
    ]

    result: aResult [
        <category: 'result dispatch'>
        timeoutTimer cancel.
        onResult ifNotNil: [:block | block value: aResult].
    ]
]


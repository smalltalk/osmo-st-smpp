"
 (C) 2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Osmo.OsmoStreamSocketBase subclass: SMPPConnection [
    | writeQueue nextSeq systemId password systemType pendingCommands |
    <category: 'SMPP-Connection'>

    createConnection: aHostname port: aPort [
        <category: 'socket'>
        ^ Sockets.StreamSocket remote: aHostname port: aPort.
    ]

    systemId: anId [
        systemId := anId
    ]

    password: aPassword [
        password := aPassword
    ]

    systemType: aType [
        systemType := aType
    ]

    scheduleBindTrx [
        | command body |

        body := SMPPBindTransceiver new
                    systemId: systemId;
                    password: password;
                    systemType: systemType;
                    version: 16r34;
                    numberingPlanIndicator: 0;
                    typeOfNumber: 0;
                    addressRange: #[];
                    yourself.
        command := SMPPCommand initWith: body.
        self scheduleCommand: command.
    ]

    scheduleCommand: aCommand [
        aCommand scheduledOn: self.
        self send: aCommand.
    ]

    send: aCommand [
        | seq key header message |

        seq := nextSeq.
        nextSeq := nextSeq + 1.

        header := SMPPPDUHeader new
            sequenceNumber: seq;
            commandId: aCommand messageType;
            commandStatus: 0;
            yourself.

        message := SMPPMessage new
                    header: header;
                    body: aCommand body;
                    yourself.

        "Remember that we want a response. TODO add timeout handling"
        pendingCommands at: seq put: aCommand.
        writeQueue nextPut: message toMessage asByteArray
    ]

    connect [
        super connect.
        nextSeq := 1.
        writeQueue := SharedQueue new.
        pendingCommands := Dictionary new.
        self scheduleBindTrx.
    ]

    sendOne [
        | msg |
        "TODO: Pill of death!"
        msg := writeQueue next.
        socket nextPutAllFlush: msg.
    ]

    dispatchOne [
        | msg |

        [
            msg := SMPPMessage readFrom: socket.
            msg body connectionDispatchOn: self with: msg
        ] on: Error do: [:e |
            e pass
        ]
    ]

    respondToEnquire: aMessage [
        | msg |
        msg := SMPPMessage new
                    header: (SMPPPDUHeader new
                                commandId: SMPPBodyBase enquireLinkResp;
                                commandStatus: 0;
                                sequenceNumber: aMessage header sequenceNumber;
                                yourself);
                    body: #[].
        writeQueue nextPut: msg toMessage asByteArray
    ]

    receviedResponse: aMessage [
        | seq command |
	"Search for a response"
        seq := aMessage header sequenceNumber.
        command := pendingCommands removeKey: seq ifAbsent: [
            "TODO: log it"
            ^false].

        command result: aMessage.
    ]
]

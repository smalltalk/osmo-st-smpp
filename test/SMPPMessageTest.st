"
 (C) 2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

TestCase subclass: SMPPMessageTest [
    <category: 'SMPP-Codec-Test'>
    <comment: 'I test reading and writing most of the SMPP header and body'>

    examplePdu [
        ^#[16r00 16r00 16r00 16r2F 16r00 16r00 16r00 16r02
           16r00 16r00 16r00 16r00 16r00 16r00 16r00 16r01
           16r53 16r4D 16r50 16r50 16r33 16r54 16r45 16r53
           16r54 16r00 16r73 16r65 16r63 16r72 16r65 16r74
           16r30 16r38 16r00 16r53 16r55 16r42 16r4D 16r49
           16r54 16r31 16r00 16r00 16r01 16r01 16r00]
    ]

    exampleBind [
        ^#[16r00 16r00 16r00 16r21 16r00 16r00 16r00 16r09
           16r00 16r00 16r00 16r00 16r00 16r00 16r00 16r02
           16r41 16r41 16r41 16r41 16r41 16r41 16r41 16r41
           16r41 16r41 16r00 16r00 16r00 16r34 16r00 16r00
           16r00]
    ]

    exampleEnquire [
        ^#[16r00 16r00 16r00 16r10 16r00 16r00 16r00 16r15
           16r00 16r00 16r00 16r00 16r00 16r00 16r00 16r03]
    ]

    exampleGenericNack [
        ^#[16r00 16r00 16r00 16r10 16r80 16r00 16r00 16r00
           16r00 16r00 16r00 16r03 16r6A 16rEC 16r9D 16rCB]
    ]

    exampleUnbind [
        ^#[16r00 16r00 16r00 16r10 16r00 16r00 16r00 16r06
           16r00 16r00 16r00 16r00 16r00 16r00 16r00 16r06]
    ]

    exampleSubmitSM [
        ^#[16r00 16r00 16r00 16r61 16r00 16r00 16r00 16r04
           16r00 16r00 16r00 16r00 16r00 16r00 16r00 16r04
           16r00 16r01 16r01 16r39 16r32 16r32 16r35 16r30
           16r30 16r31 16r00 16r01 16r01 16r34 16r30 16r30
           16r39 16r39 16r39 16r31 16r36 16r00 16r02 16r00
           16r00 16r00 16r00 16r00 16r00 16r00 16r00 16r31
           16r44 16r69 16r65 16r73 16r20 16r69 16r73 16r74
           16r20 16r65 16r69 16r6E 16r65 16r20 16r54 16r65
           16r73 16r74 16r6E 16r61 16r63 16r68 16r72 16r69
           16r63 16r68 16r74 16r21 16r20 16r20 16r32 16r30
           16r31 16r34 16r2D 16r30 16r33 16r2D 16r30 16r31
           16r5F 16r31 16r36 16r2E 16r34 16r30 16r2E 16r34
           16r32]
    ]

    exampleBindResponse [
        ^#[16r00 16r00 16r00 16r1D 16r80 16r00 16r00 16r09
           16r00 16r00 16r00 16r00 16r00 16r00 16r00 16r00
           16r53 16r4D 16r50 16r50 16r4D 16r41 16r50 16r00
           16r02 16r10 16r00 16r01 16r34]
    ]

    testReadMessage [
        | msg |
        msg := SMPPMessage readFrom: self examplePdu readStream.
        self assert: msg header commandId equals: 2.
        self assert: msg header commandStatus equals: 0.
        self assert: msg header sequenceNumber equals: 1.

        self assert: msg body systemId equals: 'SMPP3TEST'.
        self assert: msg body password equals: 'secret08'.
        self assert: msg body systemType equals: 'SUBMIT1'.
        self assert: msg body version equals: 0.
        self assert: msg body typeOfNumber equals: 1.
        self assert: msg body numberingPlanIndicator equals: 1.
        self assert: msg body addressRange equals: ''.
    ]

    testRoundTrip [
        | msg res |
        msg := SMPPMessage readFrom: self examplePdu readStream.
        res := msg toMessage asByteArray.
        self assert: res equals: self examplePdu
    ]

    testWriteMessage [
        | data |
        data := (SMPPMessage new
                    header: (SMPPPDUHeader new
                                commandId: 2;
                                commandStatus: 0;
                                sequenceNumber: 1;
                                yourself);
                    body: (self examplePdu copyFrom: 17);
                    toMessage) asByteArray.
        self assert: data equals: self examplePdu.
    ]

    testBindTrx [
        | msg |
        msg := SMPPMessage readFrom: self exampleBind readStream.
        self assert: msg body class equals: SMPPBindTransceiver.
        self assert: msg body systemId equals: 'AAAAAAAAAA'.
    ]

    testEnquireLink [
        | msg |
        msg := SMPPMessage readFrom: self exampleEnquire readStream.
        self assert: msg body class equals: SMPPEnquireLink.
    ]

    testGenericNack [
        | msg |
        msg := SMPPMessage readFrom: self exampleGenericNack readStream.
        self assert: msg body class equals: SMPPGenericNack.
    ]

    testExampleUnbind [
        | msg |
        msg := SMPPMessage readFrom: self exampleUnbind readStream.
        self assert: msg body class equals: SMPPUnbind.
    ]

    testSubmitSM [
        | msg res |
        msg := SMPPMessage readFrom: self exampleSubmitSM readStream.
        self assert: msg body class equals:SMPPSubmitSM.
        self assert: msg body shortMessage equals: 'Dies ist eine Testnachricht!  2014-03-01_16.40.42'.
        self assert: msg body sourceAddress equals: '9225001'.
        self assert: msg body destinationAddress equals: '40099916'.

        "Do round trip test"
        res := msg toMessage asByteArray.
        self assert: res equals: self exampleSubmitSM.
    ]

    testBindResponse [
        | msg res |
        msg := SMPPMessage readFrom: self exampleBindResponse readStream.
        self assert: msg body class equals: SMPPBindTransceiverResponse.
        self assert: msg body systemId equals: 'SMPPMAP'.

        "Do round trip test"
        res := msg toMessage asByteArray.
        self assert: res equals: self exampleBindResponse.
    ]
]

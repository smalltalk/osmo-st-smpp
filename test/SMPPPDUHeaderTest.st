"
 (C) 2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

TestCase subclass: SMPPPDUHeaderTest [
    <category: 'SMPP-Codec-Test'>
    <comment: 'I test reading and writing most of the SMPP header.'>

    testParse [
        | hdr |
        hdr := SMPPPDUHeader readFrom: #[0 0 0 2  0 0 0 0  0 0 0 1] readStream.
        self assert: hdr commandId equals: 2.
        self assert: hdr commandStatus equals: 0.
        self assert: hdr sequenceNumber equals: 1.
    ]

    testWrite [
        | data |
        data := (SMPPPDUHeader new
                    commandId: 2;
                    commandStatus: 0;
                    sequenceNumber: 1;
                    toMessage) asByteArray.
        self assert: data equals: #[0 0 0 2  0 0 0 0  0 0 0 1].
    ]
]

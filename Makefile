
GST_PACKAGE = gst-package
GST_CONVERT = gst-convert

CONVERT_RULES = -r'Osmo.LogManager->LogManager' \
		-r'Osmo.LogArea->LogArea'  \
		-r'Osmo.LogLevel->LogLevel' \
                -r'Osmo.TimerScheduler->TimerScheduler' \
		-r'Osmo.TLVDescription->TLVDescription' \
                -r'Sockets.StreamSocket->SocketStream' \
		-r'DateTime->DateAndTime' \
		-r'(Duration milliseconds: ``@args1) -> (Duration milliSeconds: ``@args1)' \
		-r'PP.PPCompositeParser->PPCompositeParser' \
		-r'PP.PPCompositeParserTest->PPCompositeParserTest' \
		-r'STInST.RBProgramNodeVisitor->RBProgramNodeVisitor' \
		-r'STInST.RBBracketedMethodParser->RBParser' \
		-r'Osmo.MessageBuffer->MessageBuffer' \
		-r'SystemExceptions.NotFound->NotFound' \
		-r'(``@object substrings: ``@args1)->(``@object subStrings: ``@args1)' \
		-r'(Dictionary from: ``@args1)->(Dictionary newFrom: ``@args1)' \
		-r'(``@object copyFrom: ``@args1)->(``@object copyFrom: ``@args1 to: ``@object size)' \
		-r'(``@object nl)->(``@object cr; lf)' \
		-r'(``@object methodSourceString)->(``@object sourceCode)' \
		-C -IPAGSTTests

# Can not be parsed right now..
#		-r'(``@object => ``@args1)->(``@object ==> ``@args1)'

TEST = \
	./test/SMPPPDUHeaderTest.st \
	./test/SMPPMessageTest.st

CONNECTION = \
	./connection/Extensions.st \
	./connection/SMPPConnection.st \
	./connection/SMPPCommand.st

CODEC = \
	./codec/attributes/SMPPOctetString.st \
	./codec/attributes/SMPPInteger.st \
	./codec/attributes/SMPPRegisteredDelivery.st \
	./codec/attributes/SMPPScheduleDeliveryTime.st \
	./codec/attributes/SMPPESMClass.st \
	./codec/attributes/SMPPDefaultMessageId.st \
	./codec/attributes/SMPPAddress.st \
	./codec/attributes/SMPPInterfaceVersion.st \
	./codec/attributes/SMPPSystemType.st \
	./codec/attributes/SMPPPassword.st \
	./codec/attributes/SMPPAddressNumberingPlanIndicator.st \
	./codec/attributes/SMPPValidityPeriod.st \
	./codec/attributes/SMPPValueHolder.st \
	./codec/attributes/SMPPReplaceIfPresentFlag.st \
	./codec/attributes/SMPPShortMessage.st \
	./codec/attributes/SMPPAddressTypeOfNumber.st \
	./codec/attributes/SMPPSystemId.st \
	./codec/attributes/SMPPDataCoding.st \
	./codec/attributes/SMPPPriorityFlag.st \
	./codec/attributes/SMPPProtocolId.st \
	./codec/attributes/SMPPServiceType.st \
	./codec/attributes/SMPPAddressRange.st \
	./codec/SMPPBodyBase.st \
	./codec/SMPPDeliverSM.st \
	./codec/SMPPMessage.st \
	./codec/SMPPEnquireLink.st \
	./codec/SMPPGenericNack.st \
	./codec/SMPPSubmitSM.st \
	./codec/SMPPPDUHeader.st \
	./codec/SMPPBindTransmitterBody.st \
	./codec/SMPPBindTransceiverResponse.st \
	./codec/SMPPBindTransceiver.st \
	./codec/SMPPUnbind.st

all:
	$(GST_PACKAGE) --test package.xml

convert:
	$(GST_CONVERT) $(CONVERT_RULES) -F squeak -f gst \
		-o fileout.st pharo-porting/compat_for_pharo.st \
		$(CODEC) \
		$(CONNECTION) \
		$(TEST) \
		pharo-porting/changes_for_pharo.st

